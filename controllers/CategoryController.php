<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 04.12.2018
 * Time: 22:52
 */

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Yii;

class CategoryController extends AppController
{
    public function actionIndex()
    {
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        return $this->render('index', compact('hits'));
    }

}