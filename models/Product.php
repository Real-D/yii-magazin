<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 10.11.2018
 * Time: 13:20
 */

namespace app\models;

use yii\db\ActiveRecord;


class Product extends ActiveRecord
{
    public static function TableName(){
        return 'product';
    }

    public function getCategory(){
        return $this->hasOne( Category::class, ['id' => 'category_id'] );
    }
}