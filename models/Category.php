<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 10.11.2018
 * Time: 13:11
 */

namespace app\models;

use yii\db\ActiveRecord;


class Category extends ActiveRecord
{
    public static function TebleName(){
        return 'category';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}